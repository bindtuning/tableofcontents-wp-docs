![options](../images/modern/07.options.png)

- [Layout](./layout)
- [Animations](./animations)
- [Web Part Appearance](./appearance)
- [Advanced Options](./advanced)
- [Web Part Messages](./message)