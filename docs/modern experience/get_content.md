___
### How to link other web parts to Table of Contents

Table of Contents web part detects other web parts on the page and creates the content based on the other web part's titles and items.

<p class="alert alert-warning">
<b>Note:</b> At the moment, BindTuning Table of Contents web part only supports the <b>BindTuning Accordion</b> web part as content source.
</p>

Steps to link the BindTuning Accordion to the Table of Contents:

1. Add a BindTuning Accordion web part to the page
2. Open the Accordion settings and activate anchors for web part zone or items
3. The content will be automaticaly added to the Table of Contents Web Part

![layout-options](../images/modern/20.accordion_settings.png)

![layout-options](../images/modern/19.icons.png)
___
### How to update the content from web parts

Some changes on the linked web parts may not be automatically reflected on the table of contents. If the content needs to be updated, go to edit mode and click on the refresh button. All changes will be detected and updated.

![layout-options](../images/modern/21.refresh_button.png)

___
### How to edit the content on the panel

The content retrieved from web parts may be edited. With the page in edit mode, there are inline controls on each item of the panel. The actions are:

- edit the text
- revert to the original text
- toggle visibility on/off

![layout-options](../images/modern/22.inline_controls.png)