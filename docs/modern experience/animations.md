![animations](../images/modern/24.animations.png)

Choose animations for navigation.
___
### Scroll behavior

- default
- smooth - the auto scroll to the selected zone has a smooth transition
___
### Highlight web part

- None
- Fade - A fade in/out effect will be triggered on the selected web part 
- Zoom - A zoom in/out effect will be triggered on the selected web part

___
### Highlight item

- None
- Fade - A fade in/out effect will be triggered on the selected item 
- Zoom - A zoom in/out effect will be triggered on the selected item