
![layout-options](../images/modern/14.layoutoptions.png)

___
### Hide in publish mode

Turns off the visibility of the web part in publish mode.

___
### Panel position

Choose panel's position on the page

- Web Part zone - the panel will be placed inside the web part's section 
- Fixed - the panel will be placed on a fixed position even when the user is scrolling the page

___
### Alignment

Choose where the fixed panel will be anchored

- Left
- Right

___
### Collapse panel

Choose the behavior to close (collapse) the panel

- Click on a button
- Click on a button or outside the panel

___
### Initial state

Choose the panel's intial state, when the page is loaded.

- Opened
- Closed (Collapsed)

<br>

| panel state       |  |
| ----------------- | ----------- |
| opened panel      | ![layout-options](../images/modern/17.not_collapsed.png)       |
| collapsed panel   | ![layout-options](../images/modern/18.collapsed.png)           |

___
### Title icons

Choose an icon for the headings

- None
- Custom - choose an icon from <a href="https://fontawesome.com/v4.7/icons/" target="_blank">FontAwesome</a>

___
### Item's icons

Choose an icon for the items

- None
- From Web Part - use the same icons of the web part's items
- Custom - choose an icon <a href="https://fontawesome.com/v4.7/icons/" target="_blank">FontAwesome</a>

example using custom icons from FontAwesome. 
A "tag" icon for web part title and a "circle-o" icon for the items

![layout-options](../images/modern/19.icons.png)

___
### Title font size

Choose icon to the text size for the titles

- Small
- Normal
- Large

___
### Item font size

Choose icon to the text size for the items

- Small
- Normal
- Large

___
### Text overflow

Choose the behavior when the text doesn't fit on the panel's width

- Ellipsis
- Break line