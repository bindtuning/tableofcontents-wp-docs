![webpartapperence](../images/modern/06.webpartappearence.png)

### Title Style

Choose the colors for the table of contents title.

- Default - The default gray scale palette
- Inherit from theme - Applies the colors from theme
- Custom color - Choose your own background and text colors for title. Only hexadecimal values are allowed
___
### Content Style

Choose the colors for the content

- Default - The default gray scale palette
- Inherit from theme - Applies the colors from theme
- Custom color - Choose your own custom colors for headings and items. Only hexadecimal values are allowed